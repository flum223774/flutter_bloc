import 'dart:convert';
import 'package:flutter_bloc/base/base_list_view_model.dart';
import 'package:flutter_bloc/data/repository/news_repository.dart';

class NewsViewModel extends BaseListViewModel {
  List<dynamic> allNews = [];
  final _newsRepository = NewsRepository();

  void onRefresh() async {
    setPage(1);
    allNews = await this.getData(page);
    notifyListeners();
    // refreshController.refreshCompleted();
  }

  void onLoading() async {
    setPage(page + 1);
    var data = await this.getData(page);
    allNews = [...allNews, ...data];
    notifyListeners();
    // refreshController.loadComplete();
  }

  Future<List<dynamic>> getData(int page) async {
    List<dynamic> data = [];
    await _newsRepository
        .getNews(page)
        .then((res) => data = res)
        .catchError((e) {
      print('Err, $e');
    }).whenComplete(() {
      return data;
    });
    return [];
  }
}
