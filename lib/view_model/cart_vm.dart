
import 'package:flutter/material.dart';
import 'package:flutter_bloc/models/product.dart';

class CartViewModel extends ChangeNotifier {
  List<Product> _products = [];

  List<Product> get products => _products;

  addCart(Product product) {
    _products.add(product);
    notifyListeners();
  }
}
