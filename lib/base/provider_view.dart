import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProviderView<T extends ChangeNotifier> extends StatefulWidget {
  final ValueWidgetBuilder<T> builder;
  final ChangeNotifier model;
  final Function(T model) onReady;

  const ProviderView({Key key, this.builder, this.model, this.onReady})
      : super(key: key);

  @override
  _ProviderViewState<T> createState() => _ProviderViewState<T>();
}

class _ProviderViewState<T extends ChangeNotifier> extends State<ProviderView<T>> {
  T model;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    model = widget.model;
    widget.onReady?.call(model);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
        create: (BuildContext context) => widget.model,
        child: Consumer<T>(builder: widget.builder));
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
