import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

abstract class BaseBloc {
  final _loading = BehaviorSubject<bool>.seeded(false);

  setLoading(bool loading) {
    _loading.add(loading);
  }

  Stream<bool> get loadingOnchange => _loading.stream;

  void dispose() {
    _loading.close();
  }
}

class BlocProvider<T extends BaseBloc> extends StatefulWidget {
  final ValueWidgetBuilder<T> builder;
  final BaseBloc bloc;
  final Function(T bloc) onReady;

  const BlocProvider({Key key, this.builder, this.bloc, this.onReady})
      : super(key: key);

  @override
  _BlocProviderState<T> createState() => _BlocProviderState<T>();
}

class _BlocProviderState<T extends BaseBloc> extends State<BlocProvider<T>> {
  T bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc = widget.bloc;
    widget.onReady?.call(bloc);
  }

  @override
  Widget build(BuildContext context) {
    return Provider<T>(
        create: (BuildContext context) => widget.bloc,
        child: Consumer<T>(builder: widget.builder));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    bloc.dispose();
    super.dispose();
  }
}
