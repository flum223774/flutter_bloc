import 'dart:convert';

import 'package:http/http.dart';
import 'package:flutter_bloc/helper/constants.dart';

class NewsRepository {
  Future<List<dynamic>> getNews(int page) async {
    try {
      var res = await get(urlAPINews + '/news/$page.json');
      return jsonDecode(res.body);
    } catch (e) {
      throw (e);
    }
  }
}
