import 'package:flutter_bloc/base/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:validators/validators.dart' as validator;

class LoginBloc extends BaseBloc {
  /// Objects
  final _emailObject = BehaviorSubject<String>.seeded(null);
  final _passwordObject = BehaviorSubject<String>.seeded(null);
  final _btnObject = BehaviorSubject<bool>.seeded(false);

  /// Stream
  Stream<String> get emailOnchange => _emailObject.stream;

  Stream<String> get passwordOnchange => _passwordObject.stream;

  Stream<bool> get btnOnChange => _btnObject.stream;

  void setEmail(String text) {
    _emailObject.sink.add(text);
    if (validator.isNull(text) || !validator.isEmail(text)) {
      _emailObject.sink.addError('Email trống hoặc không đúng định dạng');
    }
    _btnObject.add(checkHasError());
  }

  void setPassword(String text) {
    _passwordObject.sink.add(text);
    if (validator.isNull(text) || !validator.isLength(text, 4)) {
      _passwordObject.sink.addError('Mật khẩu phải có ít nhất 4 ký tự');
    }
    _btnObject.add(checkHasError());
  }
  bool checkHasError(){
    return _emailObject.hasError || _passwordObject.hasError;
  }

  // LoginBloc() {
  //   CombineLatestStream.combine2(_emailObject, _passwordObject, (name, pass) {
  //     return !validator.isNull(pass) &&
  //         !validator.isNull(name) &&
  //         validator.isLength(pass, 4) &&
  //         validator.isEmail(name);
  //   }).listen((enable) {
  //     _btnObject.sink.add(enable);
  //   });
  // }

  ///
  /// Action
  ///
  login() {
    print('Email: ${_emailObject.value} , Password: ${_passwordObject.value}');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _emailObject.close();
    _passwordObject.close();
    _btnObject.close();
    super.dispose();
  }
}
