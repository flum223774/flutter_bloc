import 'package:flutter/cupertino.dart';

class BlocLoading extends StatelessWidget {
  final Stream loading;
  final Widget child;

  const BlocLoading({Key key, @required this.loading, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      initialData: false,
      builder: (_, snapshot) {
        if (snapshot.data) {
          return Center(
            child: Text('Loading ...'),
          );
        }else{
          return child;
        }
      },
      stream: loading,
    );
  }
}
