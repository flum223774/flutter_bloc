import 'package:flutter/material.dart';
import 'package:flutter_bloc/base/bloc_provider.dart';
import 'package:flutter_bloc/base/provider_view.dart';
import 'package:flutter_bloc/blocs/news_bloc.dart';
import 'package:flutter_bloc/ui/screens/login_screen.dart';
import 'package:flutter_bloc/ui/widgets/loading_bloc.dart';
//import 'package:flutter_bloc/view_model/news_vm.dart';
//import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeNewsRx extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rx News'),
        centerTitle: false,
        actions: [
          IconButton(
              icon: Icon(Icons.login),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => LoginScreen()));
              })
        ],
      ),
      body: BlocProvider<NewsBloc>(
        bloc: NewsBloc(),
        onReady: (bloc) => bloc.onLoading(),
        builder: ((_, bloc, __) {
          return BlocLoading(
            loading: bloc.loadingOnchange,
            child: StreamBuilder<List<dynamic>>(
              builder: (_, snapshot) {
                if (snapshot.hasError) {
                  return Center(child: Text('Lỗi lấy dữ liệu'));
                }
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        title:
                            Text('${snapshot.data.elementAt(index)['title']}'),
                      );
                    });
              },
              stream: bloc.newsOnchange,
              initialData: [],
            ),
          );
        }),
      ),
    );
  }
}
